import cv2
import glob

"""
	programa para empiricalmente selecionar as imagens melhor segmentadas
"""

if __name__ == '__main__':

	root = 'C:/Users/gabriel/Desktop/Base/segmented/marcacoes/'
	isets = ['train/']
	ifolders = ['PNEUMONIA/']
	i = 0
	better = []


	with open('candidates.txt', 'r') as f:
		better = [x.rstrip('\n') for x in f.readlines()]					# Carrega o nome dos dados que já estão salvos

	with open('candidates.txt', 'a') as f:
		for iset in isets:
			for ifolder in ifolders:
				fns = glob.glob(root + iset + ifolder + '*g')
				for fn in fns:
					if fn in better:
						fns.remove(fn)

				for fn in fns:
					print(fn)
					im = cv2.resize(cv2.imread(fn), (256, 256))
					cv2.imshow(fn, im)
					key = cv2.waitKey(0)
					if key == ord('y'):
						better.append(fn)
						f.write("%s\n" % fn)
					elif key == ord('q'):
						quit()

					cv2.destroyWindow(fn)

	
