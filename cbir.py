import argparse
import cv2
import glob
import itertools
import numpy as np
import pandas as pd
import pickle

from datetime import datetime as dt
from sklearn.cluster import KMeans
from sklearn.neighbors import BallTree

# Computar descritores
def getDescriptors(path, fns = None):
	"""
		Computa os descritores das imagens que comporão o CBIR

		Parameters
		----------
		path: string
			Pasta com as imagens de entrada, usada quando todas as imagens estão em um mesmo diretório
		fns: list
			Lista dos caminhos das imagens q serão usadas no CBIR. Se informada, 
			a função assume que fns contém os arquivos, não path

		Returns
		-------
		descriptors: list
			Lista com os descritores das imagens
	"""
	descriptors = []
	
	surf = cv2.xfeatures2d.SURF_create()
	surf.setHessianThreshold(400)

	if fns is None:
		fns = glob.glob(path+'/*g')
	i = 0

	for fn in fns:
		name = fn.split('\\')[-1]
		print('computing descrptors for ' + fn + ': [{}/{}]'.format(i, len(fns)))
		keypoints, descrptors = surf.detectAndCompute(cv2.imread(fn, 0), None)

		descriptors.append(descrptors)
		i += 1
	
	descriptors = np.asarray(list(itertools.chain.from_iterable(descriptors)))

	return descriptors

# Gerar o dicionário visual
def generateCodebook(descriptors, size):
	# descriptors = np.hstack([x[:, :] for x in descriptors])
	# descriptors.sort(key = lambda x: x.shape[0])
	# for x in descriptors: print(x.shape)
	# quit()
	estimator = KMeans(n_clusters = size, init = 'k-means++', tol = 0.0001, verbose = 1).fit(descriptors)

	return estimator

def getVLAD(X, codebook):
    predicted = codebook.predict(X)
    centers = codebook.cluster_centers_
    labels = codebook.labels_
    k = codebook.n_clusters
   
    m, d = X.shape
    V = np.zeros([k, d])
    #computing the differences

    # for all the clusters (visual words)
    for i in range(k):
        # if there is at least one descriptor in that cluster
        if np.sum(predicted == i) > 0:
            # add the diferences
            V[i] = np.sum(X[predicted == i, :] - centers[i], axis = 0)
    

    V = V.flatten()
    # power normalization, also called square-rooting normalization
    V = np.sign(V) * np.sqrt(np.abs(V))

    # L2 normalization

    V = V / np.sqrt(np.dot(V, V))
    
    return V

# Computar os descritores VLAD
def getVLADDescriptors(path, codebook, fns = None):
	surf = cv2.xfeatures2d.SURF_create()
	surf.setHessianThreshold(400)

	descriptors = []
	image_names = []

	i = 1	
	if fns == None:
		fns = glob.glob(path+'/*g')

	for fn in fns:
		print('Computing vlad descriptors for ' + fn + ': [{}/{}]'.format(i, len(fns)))
		keypoints, descrptors = surf.detectAndCompute(cv2.imread(fn, 0), None)
		v = getVLAD(descrptors, codebook)
		descriptors.append(v)
		image_names.append(fn)
		i += 1
	descriptors = np.asarray(descriptors)

	return descriptors, image_names

# Finalmente Recuperar as imagens mais semelhantes (Padrão: 5 imagens)
def retrieveImages(path, tree, codebook):
	"""
		Encontra as 5 imagens mais semelhantes à entrada

		Parameters
		----------
		path: string
			Caminho para a imagem a ser comparada
		tree: BallTree
			indexador com as características e caminhos das imagens a serem recuperadas
		codebook: sklearn.cluster.k_means_.KMeans
			Preditor que tem o dicionário visual

		Returns
		-------
		dist: numpy_ndarray
			Array contendo as distâncias das 5 imagens mais próximas à imagem de entrada
		ind: numpy_ndarray
			Array contendo os indices das 5 imagens mais próximas à imagem de entrada no indexador 
	"""
	surf = cv2.xfeatures2d.SURF_create()
	surf.setHessianThreshold(400)

	im = cv2.imread(path, 0)
	kp, desc = surf.detectAndCompute(im, None)

	v = getVLAD(desc, codebook)

	dist, ind = tree.query(v.reshape(1, -1), 5)

	return dist, ind

def save(output, output_name):
	with open(output_name, 'wb') as f:
		pickle.dump(output, f)
		f.close()

def compute(X, preffix = '', suffix = ''):
	"""
		Computa todos os requisitos necessários

		Parameters
		----------
		X : list
			Lista com os nomes das imagens que serão recuperadas
		preffix : string
			Prefixo dos arquivos de saída
		suffix : string
			Sufixo dos arquivos de saída
		
		Returns
		-------
		out : tuple
			(ballTree, codebook): Estruturas necessárias para a recuperação das imagens

		Example
		--------
		>>> (bt, vd) = compute(X, suffix = 'test') 
		Arquivos gerados: descriptors_test.pickle, codebook_test.pickle,  vlad_test.pickle, ballTree_test.pickle
	"""
	output_names = ['_descriptors_', '_codebook_', '_vlad_', '_ballTree_']
	output_names = [preffix + x + suffix + '.pickle' for x in output_names]

	print("Computing Descriptors")	
	descritores = getDescriptors('', fns = X)
	save(descritores, output_names[0])

	print("Generating Codebook")
	codebook = generateCodebook(descritores, 64)
	save(codebook, output_names[1])
	
	print('Computing VLAD descriptors')
	VLAD, image_names = getVLADDescriptors('', codebook, fns = X)
	save([VLAD, image_names, X], output_names[0])
	
	print('Making ball tree')
	ballTree = BallTree(VLAD)
	save([image_names, ballTree, X], output_names[0])

	print("Arquivos gerados:", output_names)
	
	return (ballTree, vladdesc)

# Gerar o indexador
if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('-c', '--computar', action = 'store_true', required = False, default = False,
						help = 'computar ou apenas carregar os descritores')
	parser.add_argument('-d', '--descritor', action = 'store', required = False, default = None,
						help = 'Arquivo contendo os descritores')
	parser.add_argument('-cd', '--codebook', action = 'store', required = False, default = None,
						help = 'Arquivo contendo o dicionário visual')
	parser.add_argument('-vd', '--vladdesc', action = 'store', required = False, default = None,
						help = 'Arquivo contendo os descritores VLAD')
	parser.add_argument('-b', '--ballTree', action = 'store', required = False, default = None,
						help = 'Arquivo contendo os indexador do VLAD')
	parser.add_argument('-s', '--singleEntry', action = 'store', required = False, default = None,
					help = 'Arquivo contendo o caminho para todas as entradas')
	# help(compute)
	lines = []
	with open('parsed_candidates.txt', 'r') as f:
		lines = [x.rstrip('\n') for x in f.readlines()]
		f.close()
	compute(lines, preffix = 'pneumonia')
	# args = vars(parser.parse_args())
	# # print(type(args['arquivo']))

	# base = 'C:/Users/gabriel/Desktop/oneshape'
	# # teste = 'C:/Users/gabriel/Desktop/MCUCXR_0003_0.png'
	
	# if args['computar'] == True:
	# 	print("Computing Descriptors")
	# 	d = getDescriptors(base)

	# 	filename = 'descriptors_'+str(dt.date(dt.now()))+'.pickle'
	# 	with open(filename, 'wb') as f:
	# 		pickle.dump(d, f)


	# 	print("Generating Codebook")
	# 	v = generateCodebook(d, 64)
	# 	filename = 'codebook_'+str(dt.date(dt.now()))+'.pickle'
	# 	with open(filename, 'wb') as f:
	# 		pickle.dump(v, f)

	# 	print('Computing VLAD descriptors')
	# 	VLAD, image_names = getVLADDescriptors(base, v)
	# 	filename = 'vlad_'+str(dt.date(dt.now()))+'.pickle'		
	# 	with open(filename, 'wb') as f:
	# 		pickle.dump([VLAD, image_names, base], f)

	# 	print('Making ball tree')
	# 	ballTree = BallTree(VLAD)
	# 	filename = 'ballTree_'+str(dt.date(dt.now()))+'.pickle'		
	# 	with open(filename, 'wb') as f:
	# 		pickle.dump([image_names, ballTree, base], f)
	# else:
	# 	if args['descritor'] is None:
	# 		parser.error('Forneça um arquivo válido para')
	# 	else:
	# 		print("Loading Descriptors")
	# 		d = pickle.load(open(args['descritor'], 'rb'))
	# 		print('Loading Codebook')
	# 		v = pickle.load(open(args['codebook'], 'rb'))
	# 		print('Loading VLAD descriptors')
	# 		[VLAD, image_names, base] = pickle.load(open(args['vladdesc'], 'rb'))
	# 		print('Making ball tree')
	# 		print('Loading Ball Tree Index')
	# 		bt = pickle.load(open(args['ballTree'], 'rb'))

	# paths = bt[0]
	# ballTree = bt[1]
	# base = bt[2]

	# print('Retrieving Data')
	# dist, ind = retrieveImages(teste, ballTree, v)
	# ind = list(itertools.chain.from_iterable(ind))
	# # print(type(dist))
	# dist = list(itertools.chain.from_iterable(dist))

	# # paths = ballTree

	# output = []
	# for i in ind:
	# 	print(paths[i])

