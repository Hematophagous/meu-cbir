import cv2
import numpy as np
import pickle

from cbir import retrieveImages

"""
Programa auxiliar

"""

print("Lendo Descritores")
descriptor_name = ''							# Preencehr com o nome do arquivo contendo os descritores
d = pickle.load(open(descriptor_name, 'rb'))

print('Lendo Codebook')
codebook_name = ''								# Preencher com o nome do arquivo contendo o codebook
v = pickle.load(open(codebook_name, 'rb'))

print('Lendo descritores VLAD')
vladdesc_name = ''								# Preencher com o nome do arquivo contendo os descritores VLAD
[VLAD, image_names, base] = pickle.load(open(vladdesc_name, 'rb'))

print('Lendo o indexador Ball Tree')
bt_name = ''									# Preencher com o nome do arquivo contendo o indexador
bt = pickle.load(open(bt_name, 'rb'))

paths = bt[0]
ballTree = bt[1]
base = bt[2]

root = 'C:/Users/gabriel/Desktop/Base/resized_png/'
isets = ['test/' 'train/']
ifolders = ['NORMAL/', 'PNEUMONIA/']

print('Retrieving Data')

for iset in isets:
	for ifolder in ifolders:
		fns = glob.glob(root + iset + ifolder + '*g')
		for fn in fns:
			name = fn.split('\\')[-1]
			dist, ind = retrieveImages(teste, ballTree, v)
			ind = list(itertools.chain.from_iterable(ind))
			# print(type(dist))
			dist = list(itertools.chain.from_iterable(dist))

			# paths = ballTree

			with open(name+'.txt', 'w') as f:
				for i in ind:
					f.write('%s\n' % paths[i])
				f.close()