with open('candidates.txt', 'r') as f:
	lines = [x.rstrip('\n').replace('pred_', '') for x in f.readlines()]
	lines = [x.replace('segmented/marcacoes', 'resized_png') for x in lines]
	f.close()

with open('parsed_candidates.txt', 'w') as f:
	for line in lines:
		f.write("%s\n" % line)
